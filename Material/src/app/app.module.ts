import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { MicroMatCardComponent } from './micro-mat-card/micro-mat-card.component';
    
import { MatCardModule} from '@angular/material/card';  
export const APP_ROUTES: Routes = [
  {
    path: '',
    component: AppComponent
  }
];

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    MatCardModule,  
    RouterModule.forRoot(APP_ROUTES)
  ],
  declarations: [
    AppComponent,
    MicroMatCardComponent
  ],
  providers: [],
  bootstrap: [
      AppComponent
  ]
})
export class AppModule { }
