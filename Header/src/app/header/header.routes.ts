import { Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';

export const HEADER_ROUTES: Routes = [
    {
      path: '',
      component: HeaderComponent
    }
];
