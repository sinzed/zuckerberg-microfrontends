import { HttpClient } from '@angular/common/http';
import {Component, ViewChild, ViewContainerRef, Inject, Injector, ComponentFactoryResolver, OnInit, SimpleChanges, ViewEncapsulation} from '@angular/core';
import { Route, Router } from '@angular/router';


@Component({
  selector: 'mfeHeader',
  templateUrl: './header.component.html',
  styleUrls: ["header.component.css"],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent {
  constructor(private router: Router){
  }
  public items: Array<{text:string, link:string}> = [];
  ngOnInit(){
    // if(window['mfeHeader'] && window['mfeHeader']['items']){
    //   this.items =  window['mfeHeader']['items'];
    // }
  }
  navigate(link:string){
    this.router.navigateByUrl(link);
  }

}
