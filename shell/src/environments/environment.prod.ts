export const environment = {
  production: false,
  mfeHeader: 'http://market.towist.com/libs/header/remoteEntry.js',
  mfeFacebookUrl: 'http://market.towist.com/libs/facebook/remoteEntry.js',
  mfeInstagramUrl: 'http://market.towist.com/libs/instagram/remoteEntry.js',
  mfeLoginUrl: 'http://market.towist.com/libs/login/remoteEntry.js',
};