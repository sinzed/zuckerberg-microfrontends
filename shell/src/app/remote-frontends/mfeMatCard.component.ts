import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver, Injector, ElementRef, Renderer2, ViewEncapsulation } from '@angular/core';
import { loadRemoteModule } from '@angular-architects/module-federation';
import { PluginOptions } from './plugin';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'mfeMatCard',
    template: `
        <div #placeHolderHeader>
            <ng-content #contentWrapper></ng-content>
        </div>
        `,
    styleUrls:["mfeMatCard.component.css"],
    encapsulation: ViewEncapsulation.None,
})
export class MfeMatCardComponent{
    @ViewChild("placeHolderHeader", {read: ViewContainerRef})
    viewContainer: ViewContainerRef;
    @ViewChild('placeHolderHeader') contentWrapper:ElementRef;
    constructor(
      private injector: Injector,
      private cfr: ComponentFactoryResolver,
      private renderer: Renderer2, 
      private host: ElementRef

      ) { }

    options: PluginOptions;
    async ngAfterViewInit(){
        const result = await this.lookup()
        let Component = await loadRemoteModule(result).then(m => m[result.componentName]);
        const factory = this.cfr.resolveComponentFactory(Component);

        const  compRef = factory.create(this.injector);
        const element = this.viewContainer.element;

        // this.renderer.removeChild(this.host.nativeElement, this.viewContainer.element.nativeElement);
        // compRef.instance["element"] = element.nativeElement.innerHTML

        this.viewContainer.insert(compRef.hostView);
    }
    lookup(): Promise<PluginOptions> {
        return Promise.resolve(
            {
                remoteEntry: environment.mfeMaterial,
                remoteName: 'mfeMaterial',
                exposedModule: './MicroMatCard',
    
                displayName: 'MicroMatCard',
                componentName: 'MicroMatCardComponent'
            } as PluginOptions);
      }
}

