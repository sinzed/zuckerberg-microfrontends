import { Component, Input, OnChanges, ViewChild, ViewContainerRef, ComponentFactoryResolver, Injector, Type, ViewChildren } from '@angular/core';
import { loadRemoteModule } from '@angular-architects/module-federation';
import { PluginOptions } from './plugin';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'mfeInstagram',
    template: `
        <div #placeHolder></div>
    `
})
export class MfeInstgramComponent implements OnChanges {
    @ViewChild('placeHolder', { read: ViewContainerRef})
    viewContainer: ViewContainerRef;

    constructor(
      private injector: Injector,
    //   private viewContainer: ViewContainerRef,
      private cfr: ComponentFactoryResolver) { }

    // @Input() options: PluginOptions;
    options2: PluginOptions;
    async ngOnInit(){
        this.options2 = await this.lookup();
        const Component = await loadRemoteModule(this.options2)
            .then(m => m[this.options2.componentName]);
        const factory = this.cfr.resolveComponentFactory(Component);
        // console.log("containers", this.viewContainer);
        const componentRef = factory.create(this.injector);
        this.viewContainer.insert(componentRef.hostView);
        // const compRef = this.viewContainer.createComponent(factory,0, this.injector);

    }
    async ngOnChanges() {
        // this.viewContainer.clear();
        // // this.options = await this.lookup();
        // this.options2 = await this.lookup();
        // // const Component = await loadRemoteModule(this.options)
        // //     .then(m => m[this.options.componentName]);
        // const Component = await loadRemoteModule(this.options2)
        //     .then(m => m[this.options2.componentName]);

        // // Ivy --> ViewEngine
        // const factory = this.cfr.resolveComponentFactory(Component);
        // const componentRef = factory.create(this.injector);
        // this.viewContainer.insert(componentRef.hostView);

        // const compRef = this.viewContainer.createComponent(factory, null, this.injector);


    }

    lookup(): Promise<PluginOptions> {
        return Promise.resolve(
            {
                remoteEntry: environment.mfeInstagramUrl,
                remoteName: 'mfeInstagram',
                exposedModule: './App',
    
                displayName: 'App',
                componentName: 'AppComponent'
            } as PluginOptions);
      }
}

