import { Component, ViewChild, ViewContainerRef, ComponentFactoryResolver, Injector, ViewEncapsulation } from '@angular/core';
import { loadRemoteModule } from '@angular-architects/module-federation';
import { PluginOptions } from './plugin';
import { environment } from 'src/environments/environment';

@Component({
    selector: 'mfeHeader',
    template: `
        <div #placeHolderHeader></div>
        <ng-template #element></ng-template>`,
    styles:["#container ul {background-color:yellow;}"],
    styleUrls: ['mfeHeader.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class MfeHeaderComponent{
    @ViewChild("placeHolderHeader", {read: ViewContainerRef})
    viewContainer: ViewContainerRef;
    constructor(
      private injector: Injector,
      private cfr: ComponentFactoryResolver) { }

    options: PluginOptions;
    async ngOnInit(){
        const result = await this.lookup()
        let Component = await loadRemoteModule(result).then(m => m[result.componentName]);
        const factory = this.cfr.resolveComponentFactory(Component);

        const  compRef = factory.create(this.injector);
        compRef.instance["items"] = [{text:"home", link:"/"},
        {text:"facebook", link:"/"},
        {text:"login", link:"/login"}]
        this.viewContainer.insert(compRef.hostView);
    }
    lookup(): Promise<PluginOptions> {
        return Promise.resolve(
            {
                remoteEntry: environment.mfeHeader,
                remoteName: 'mfeHeader',
                exposedModule: './Header',
    
                displayName: 'App',
                componentName: 'HeaderComponent'
            } as PluginOptions);
      }
}

